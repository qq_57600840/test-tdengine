package com.example.testtdengine.controller;

import com.example.testtdengine.domain.mysql.Test;
import com.example.testtdengine.service.TestService;
import com.example.testtdengine.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author wz
 * @title
 * @date 2025/1/14 10:00
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService testService;

    @PostMapping("/getById")
    public ResponseData getById(@RequestParam("id") Integer id) {
        ResponseData result = ResponseData.ok();
        Test test = testService.getById(id);
        result.putDataValue("data", test);
        return result;
    }

    @PostMapping("/save")
    public ResponseData save(@RequestBody Test test) {
        test.setTs(new Timestamp(new Date().getTime()));
        testService.save(test);
        return ResponseData.ok();
    }

    @GetMapping("/getAll")
    public ResponseData getAll() {
        ResponseData result = ResponseData.ok();
        List<Test> testList = testService.selectTestAll();
        result.putDataValue("data", testList);
        return result;
    }
}
