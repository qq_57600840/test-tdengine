package com.example.testtdengine.taos.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.example.testtdengine.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.time.LocalDateTime;


/**
 * @author wz
 * @title taos基础类
 * @date 2025/1/10 9:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseTaosModel<T extends Model<T>> extends Model<T> implements TaosBaseInterface {

    private static final long serialVersionUID = 2537647301574172972L;

    /** 时间戳 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp ts;

    /** 数据库名 */
    @JsonIgnore
    @TableField(exist = false)
    private String databaseName;

    /** 超级表名 */
    @JsonIgnore
    @TableField(exist = false)
    private String stableName;

    /** 表名 */
    @JsonIgnore
    @TableField(exist = false)
    private String tableName;

    /** insertValue相关语句 */
    @JsonIgnore
    @TableField(exist = false)
    private String insertValue;

    /** insertTag相关语句 */
    @JsonIgnore
    @TableField(exist = false)
    private String insertTag;

    /** 建表相关语句 */
    @JsonIgnore
    @TableField(exist = false)
    private String createValues;

    /** 批量新增语句 */
    @JsonIgnore
    @TableField(exist = false)
    private String batchSql;

    /**
     * 时间戳构造方法
     *
     * @param ts 时间戳
     */
    public void setTs(Timestamp ts) {
        this.ts = ts;
    }

    /**
     * 时间戳构造方法
     *
     * @param ts 时间戳
     */
    public void setTs(Long ts) {
        this.ts = new Timestamp(ts);
    }

    /**
     * 时间戳构造方法
     *
     * @param ts 时间戳
     */
    public void setTs(LocalDateTime ts) {
        this.ts = Timestamp.valueOf(ts);
    }

    /**
     * 获取时间戳
     *
     * @return 时间戳
     */
    public Long getTsLong(){
        if (ObjectUtils.isNotEmpty(ts)) {
            return ts.getTime();
        }
        return null;
    }

    /**
     * 获取时间戳
     *
     * @return 时间戳
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getTsLocalDateTime() {
        if (ObjectUtils.isNotEmpty(ts)) {
            return ts.toLocalDateTime();
        }
        return null;
    }

    /**
     * 初始化相关语句方法
     *
     * @param baseTaosModel taos基础实体类
     */
    public void init(BaseTaosModel<?> baseTaosModel) {
        // 如果时间戳为空，默认当前时间
        if(ObjectUtils.isEmpty(this.ts)){
            this.ts = new Timestamp(System.currentTimeMillis());
        }
        // 如果数据库或超级表为空，默认从@TableName获取
        if (StringUtils.isEmpty(this.stableName)) {
            this.databaseName = getDatabaseName(baseTaosModel);
        }
        if (StringUtils.isEmpty(this.stableName)) {
            this.stableName = getSTableName(baseTaosModel);
        }
        // 获取value和tag语句
        this.insertValue = getInsertValue(baseTaosModel);
        this.insertTag = getInsertTag(baseTaosModel);
        this.createValues = getCreateSql();
    }

    /**
     * 重写构造方法，初始化成员变量，减少taos的null值存储
     */
    public BaseTaosModel() {
        setStringFieldsToEmpty(this);
    }

}
