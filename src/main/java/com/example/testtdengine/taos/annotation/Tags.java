package com.example.testtdengine.taos.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wz
 * @title 反射获取tags注解，所有tags属性添加
 * @date 2025/1/10 9:18
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Tags {

    /**
     * 标注tags的顺序
     *
     * @return 该tags的顺序
     */
    int value() default Integer.MAX_VALUE;
}
