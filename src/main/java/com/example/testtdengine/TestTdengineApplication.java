package com.example.testtdengine;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.testtdengine.mapper"})
public class TestTdengineApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestTdengineApplication.class, args);
    }

}
