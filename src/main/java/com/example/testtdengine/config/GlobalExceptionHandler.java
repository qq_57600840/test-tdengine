package com.example.testtdengine.config;

import com.example.testtdengine.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author wz
 * @title 跨域
 * @date 2025/1/10 9:18
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseData globalException(Exception e) {
        log.info("GlobalExceptionHandler...");
        e.printStackTrace();
        ResponseData result = ResponseData.resultError("系统异常请联系管理员或系统开发人员");
        return result;
    }

}
