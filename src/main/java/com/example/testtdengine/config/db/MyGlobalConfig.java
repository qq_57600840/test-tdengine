package com.example.testtdengine.config.db;

import com.baomidou.mybatisplus.core.config.GlobalConfig;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:18
 */
public class MyGlobalConfig extends GlobalConfig {
    @Autowired
    private CustomSqlSessionTemplate sqlSessionTemplate;

    private static CustomSqlSessionTemplate customSqlSessionTemplate;

    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        return customSqlSessionTemplate.getSqlSessionFactory();
    }

    @PostConstruct
    public void init() {
        MyGlobalConfig.customSqlSessionTemplate = sqlSessionTemplate;
    }
}
