package com.example.testtdengine.config.db;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:18
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    protected Object determineCurrentLookupKey() {
        return DbContextHolder.getDbType();
    }
}
