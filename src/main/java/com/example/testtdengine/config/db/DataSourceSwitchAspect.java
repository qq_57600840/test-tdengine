package com.example.testtdengine.config.db;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:18
 */
@Component
@Order(value = -100)
@Slf4j
@Aspect
public class DataSourceSwitchAspect {

    @Pointcut("execution(* com.example.testtdengine.mapper.mysql..*.*(..))")
    private void mysqlAspect() {
    }

    @Pointcut("execution(* com.example.testtdengine.mapper.tdengine..*.*(..))")
    private void tdengineAspect() {
    }


    @Before("mysqlAspect()")
    public void mysql() {
        log.info("切换到mysql 数据源...");
        DbContextHolder.setDbType(DBTypeEnum.mysql);
    }

    @Before("tdengineAspect()")
    public void tdengine() {
        log.info("切换到tdengine 数据源...");
        DbContextHolder.setDbType(DBTypeEnum.tdengine);
    }

    @After("mysqlAspect()")
    public void clear1() {
        log.info("清除mysql数据源...");
        DbContextHolder.clearDbType();
    }

    @After("tdengineAspect()")
    public void clear2() {
        log.info("清除tdengine数据源...");
        DbContextHolder.clearDbType();
    }

}
