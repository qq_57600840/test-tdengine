package com.example.testtdengine.config.db;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:18
 */
public enum DBTypeEnum {
    mysql("mysql"),
    tdengine("tdengine");
    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
