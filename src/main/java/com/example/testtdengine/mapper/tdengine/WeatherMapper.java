package com.example.testtdengine.mapper.tdengine;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.example.testtdengine.domain.tdengine.Weather;
import com.example.testtdengine.taos.mapper.BaseTaosMapper;

/**
 * @author wz
 * @title
 * @date 2025/1/10 8:56
 */
@DS("tdengine")
public interface WeatherMapper extends BaseTaosMapper<Weather> {

}
