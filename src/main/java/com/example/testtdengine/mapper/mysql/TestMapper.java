package com.example.testtdengine.mapper.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.testtdengine.domain.mysql.Test;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wz
 * @title
 * @date 2025/1/14 9:57
 */
@Repository
public interface TestMapper extends BaseMapper<Test> {
    @Select("select t.id, t.name, t.ts from test t")
    List<Test> selectTestAll();
}
