package com.example.testtdengine.domain.parameter;


import lombok.Data;

/**
 * @author wz
 * @title
 * @date 2025/1/14 15:14
 */
@Data
public class WeatherParameter {

    // 温度
    private Float temperature;
    // 湿度
    private Float humidity;
    // 位置
    private String location;

    private int groupid;
}
