package com.example.testtdengine.domain.tdengine;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.testtdengine.taos.annotation.Tags;
import com.example.testtdengine.taos.model.BaseTaosModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author wz
 * @title
 * @date 2025/1/10 8:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "test.weather")
@AllArgsConstructor
@NoArgsConstructor
public class Weather extends BaseTaosModel<Weather> {

    // 温度
    private Float temperature;
    // 湿度
    private Float humidity;
    // 位置
    @Tags
    private String location;
    // 组id
    @Tags
    @TableField("groupid")
    private int groupid;
}
