package com.example.testtdengine.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.example.testtdengine.domain.tdengine.Weather;
import com.example.testtdengine.taos.service.BaseTaosService;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:13
 */
@DS("tdengine")
public interface WeatherService extends BaseTaosService<Weather> {
}
