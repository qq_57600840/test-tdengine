package com.example.testtdengine.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.testtdengine.domain.mysql.Test;
import com.example.testtdengine.mapper.mysql.TestMapper;
import com.example.testtdengine.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wz
 * @title
 * @date 2025/1/14 9:59
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public List<Test> selectTestAll() {
        return testMapper.selectTestAll();
    }
}
