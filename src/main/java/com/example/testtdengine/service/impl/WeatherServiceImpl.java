package com.example.testtdengine.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.example.testtdengine.domain.tdengine.Weather;
import com.example.testtdengine.mapper.tdengine.WeatherMapper;
import com.example.testtdengine.service.WeatherService;
import com.example.testtdengine.taos.service.impl.BaseTaosServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author wz
 * @title
 * @date 2025/1/10 9:13
 */
@DS("tdengine")
@Service
public class WeatherServiceImpl extends BaseTaosServiceImpl<WeatherMapper, Weather> implements WeatherService {
}
