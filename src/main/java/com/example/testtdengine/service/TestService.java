package com.example.testtdengine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.testtdengine.domain.mysql.Test;

import java.util.List;

/**
 * @author wz
 * @title
 * @date 2025/1/14 9:58
 */
public interface TestService extends IService<Test> {

    List<Test> selectTestAll();
}
